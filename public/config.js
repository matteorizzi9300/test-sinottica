var appConfig = {
    'basemapImage': "background.jpg",
    'parks': {
        'spinaVerde':{
            'nome': "Parco Regionale Spina Verde",
            'url': "https://insubriparksturismo.eu/parchi/7xfXeonyNJjJ3CIcUCRQ",
            'marker': "spinaMarker.png"
        },
        'pineta': {
            'nome': "Parco Pineta",
            'url': "https://insubriparksturismo.eu/parchi/s1e8NIHT9onxO0qj8IRK",
            'marker': "pinetaMarker.png"
        },
        'campo': {
            'nome': "Parco Regionale Campo dei Fiori",
            'url': "https://insubriparksturismo.eu/parchi/R65es1tYta1Ql7BC1yKw",
            'marker': "campoMarker.png"
        },
        'gole':{
            'nome': "Parco Gole della Breggia",
            'url': "https://insubriparksturismo.eu/parchi/rClJ8uFOCJNLxE9v9JJY",
            'marker': "goleMarker.png"
        },
        'penz':{
            'nome': "Parco del Penz",
            'url': "https://insubriparksturismo.eu/parchi/y9jpmaVGCBXxZQF6pLyh",
            'marker': "penzMarker.png"
        },
        'style': {
            'color': 'white',
            'backgroundColor': 'green',
        }
    },
    'stazioni': {
        'varese1': {
            'nome': "Stazione Varese",
            'marker': "stazione.png",
            'url': "https:test.com"
        },
        'varese2': {
            'nome': "Stazione Varese 2",
            'marker': "stazione.png",
            'url': "https:test.com"
        },
        'gole': {
            'nome': "Stazione Gole",
            'marker': "stazione.png",
            'url': "https:test.com"
        },
        'como1': {
            'nome': "Stazione Como",
            'marker': "stazione.png",
            'url': "https:test.com"
        },
        'como2': {
            'nome': "Stazione Como 2",
            'marker': "stazione.png",
            'url': "https:test.com"
        },
        'pineta': {
            'nome': "Stazione Pineta",
            'marker': "stazione.png",
            'url': "https:test.com"
        },
        'style': {
            'color': 'white',
            'backgroundColor': 'black'
        }
    },
    'aereoporti':{
        "linate":{
            'nome': "Aereoporto di Linate",
            'marker': 'freccia-aeroporto-down.png',
        },
        "malpensa":{
            'nome': "Aereoporto di Milano-Malpensa",
            'marker': 'freccia-aeroporto-down.png',
        },
        "lugano": {
            'nome': "Aereoporto di Lugano-Agno",
            'marker': 'freccia-aeroporto.png',
        },
        "style":{
            'color': 'white',
            'backgroundColor': 'green'
        }
    },
    'comuni':{
        'milano':{
            'nome': "Milano",
            'marker': 'freccia-milano.png'
        },
        'mendrisio':{
            'nome': "Mendrisio",
            'marker': 'centro-mendrisio.png',
        },
        'como':{
            'nome': "Como",
            'marker': 'centro-como.png',
        },
        'varese':{
            'nome': "Varese",
            'marker': 'centro-varese.png',
        },
        'lugano':{
            'nome': "Lugano",
            'marker': 'freccia-lugano.png',
        },
        "style":{
            'color': 'white',
            'backgroundColor': 'blue'
        }
    }
}